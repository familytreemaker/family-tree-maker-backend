# How to deploy?

See https://gitlab.com/familytreemaker/family-tree-maker-deployment

---

# Legacy Notes Below

# How to use?

The family-tree-maker repo is the frontend
This repo is the backend.

First, run the backend:

```
git checkout git@gitlab.com:sureshgururajan/family-tree-maker-backend.git
npm install
npm start
```

Then run the frontend.
See https://gitlab.com/sureshgururajan/family-tree-maker/-/blob/main/README.md

# Certs

Follow guide on https://certbot.eff.org/lets-encrypt/ubuntufocal-nginx

```
sudo snap install --classic certbot
```

```
sudo ln -s /snap/bin/certbot /usr/bin/certbot
```

Auto-configure nginx configuration:

```
sudo certbot --nginx
```

# Nginx server configuration

```bash
sudo mkdir -p /var/www/www.yoursite.com/html

sudo chown -R $USER:$USER /var/www/www.yoursite.com/html
```

```bash
nano /var/www/example.com/html/index.html
```

```html
<html>
  <head>
    <title>Welcome to Example.com!</title>
  </head>
  <body>
    <h1>Success! The example.com server block is working!</h1>
  </body>
</html>
```

```bash
sudo cp /etc/nginx/sites-available/default /etc/nginx/sites-available/www.yoursite.com
```

## Verifying Bearer Tokens Sent from frontend

Create an "adminconfig.json" file at the `project root/config` directory. Format of this file:

```
{
  "type": "service_account",
  "project_id": "xx",
  "private_key_id": "xx",
  "private_key": "xx",
  "client_email": "xx",
  "client_id": "xx",
  "auth_uri": "https://accounts.google.com/o/oauth2/auth",
  "token_uri": "https://oauth2.googleapis.com/token",
  "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
  "client_x509_cert_url": "xx"
}
```

You can find this info in your firebase project configuration.

See https://firebase.google.com/docs/admin/setup and https://firebase.google.com/docs/auth/admin/verify-id-tokens

## Docker

### Dev Image

```bash
docker build -t familytreemaker-backend:dev .  # Development
docker build -t familytreemaker-backend:prod . # Production

docker run -it --rm  -p 5000:5000 familytreemaker-backend:dev -e NEO4J_USER="xx" -e NEO4J_PASSWORD="xx" -e NEO4J_URI="xx" -e NEO4J_DB="xx" # Development
docker run -it --rm  -p 5000:5000 familytreemaker-backend:prod -e NEO4J_USER="xx" -e NEO4J_PASSWORD="xx" -e NEO4J_URI="xx" -e NEO4J_DB="xx" # Production
```

```bash
docker push sureshgururajan/familytreemaker-backend:latest
```

```bash
minikube dashboard
minikube tunnel
```
