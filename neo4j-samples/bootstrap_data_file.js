const data = require("../datafile.json");
const neo4j = require("neo4j-driver");

const user = process.env.NEO4J_USER;
const password = process.env.NEO4J_PASSWORD;
const uri = process.env.NEO4J_URI;
const db = process.env.NEO4J_DB;

const driver = neo4j.driver(uri, neo4j.auth.basic(user, password));
const session = driver.session({
  database: db,
});

async function createAllData() {
  try {
    for (let key in data) {
      const personName = data[key]["name"];
      const personID = data[key]["id"].replace(/-/g, "_");
      const primaryKey = personName.split(" ")[0].split(",")[0].split(".")[0];

      if (personName === "") {
        continue;
      }

      const query = `CREATE (${primaryKey}:Person {name: $name, personID: $id})`;

      // console.log(query);

      await session.run(query, {
        name: personName,
        id: personID,
      });
    }
  } catch (ex) {
    console.log("exception", ex);
  }
}

async function addSpouseRelationships() {
  try {
    for (let key in data) {
      const person = data[key];
      const personName = person["name"];
      const personID = person["id"].replace(/-/g, "_");

      if (personName === "") {
        continue;
      }

      const spouse = data[person["spouse"]];

      if (spouse && spouse["name"]) {
        const spouseName = spouse["name"];
        const spouseID = spouse["id"].replace(/-/g, "_");
        if (spouseName === "") {
          continue;
        }

        const query = `MATCH(a), (b) WHERE a.personID="${personID}" AND b.personID="${spouseID}" MERGE (a)-[:SPOUSE_OF]-(b) RETURN a,b`;

        const result = await session.run(query);
        const singleRecord = result.records[0];
        const node = singleRecord.get(0);
        // console.log(node.properties);
      }
    }
  } catch (ex) {
    console.log("exception", ex);
  }
}

async function addChildRelationships() {
  try {
    for (let key in data) {
      const person = data[key];
      const personName = person["name"];
      const personID = person["id"].replace(/-/g, "_");

      if (personName === "") {
        continue;
      }

      const children = person["children"];
      if (children.length > 0) {
        for (let child of children) {
          const childData = data[child];
          const childID = child.replace(/-/g, "_");
          const query = `MATCH(a), (b) WHERE a.personID="${personID}" AND b.personID="${childID}" MERGE (a)-[:PARENT_OF]->(b)-[:CHILD_OF]->(a) RETURN a,b`;
          // console.log(query);
          const result = await session.run(query);
          const singleRecord = result.records[0];
          const node = singleRecord.get(0);
          // console.log(node.properties);
        }
      }
    }
  } catch (ex) {
    console.log("exception", ex);
  }
}

async function main() {
  try {
    await createAllData();
    await addSpouseRelationships();
    await addChildRelationships();
    await session.close();
    await driver.close();
  } catch (ex) {
    console.log("exception ", ex);
  }
}

main()
  .then(() => console.log("exiting"))
  .catch((ex) => console.log("exception", ex));
