const neo4j = require("neo4j-driver");

const user = process.env.NEO4J_USER;
const password = process.env.NEO4J_PASSWORD;
const uri = process.env.NEO4J_URI;
const db = process.env.NEO4J_DB;

const driver = neo4j.driver(uri, neo4j.auth.basic(user, password));
const session = driver.session({
  database: db,
});
const personName = "Liam";

async function tryer() {
  try {
    const query = `CREATE (Liam:person {name: $person}) RETURN Liam`;
    // const query = `MATCH(n) RETURN (n)`;

    const result = await session.run(query, { person: personName });
    // const result = await session.run(query);

    const singleRecord = result.records[0];
    const node = singleRecord.get(0);

    console.log(node.properties);
  } finally {
    await session.close();
  }

  // on application exit:
  await driver.close();
}

tryer()
  .then(() => console.log("done"))
  .catch((ex) => console.log("exception", ex));
